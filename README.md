**Module provide nominatium geocoder to geolocation_leaflet module**

When you choose a widget Geolocation Leaflet - Geocoding and Map in form display area and click settings 
you can choose for LEAFLET SETTINGS area Gecoder plugin. This module add new geocoder Nominatim.  

This module depend on geolocation 2.x, module provide geolocation_leaflet module require by this module.

**Install**
composer require 'drupal/geolocation:2.x-dev'
composer require 'drupal/geolocation_nominatim'

**after install turn on:**
- Geolocation Module
- Geolocation Leaflet module

**Use**
1. Add Geolocation field to your content type
2. In Form display choose Geolocation Leaflet - Geocoding and Map widget.
3. In settings this widget you get new gecoder Nominatim plugin.

