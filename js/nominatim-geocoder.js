/**
 * @file
 * Javascript for the Google Geocoding API geocoder.
 */


(function ($, Drupal) {
  'use strict';

  if (typeof Drupal.geolocation.geocoder === 'undefined') {
    return false;
  }

  drupalSettings.geolocation.geocoder.nominatim = drupalSettings.geolocation.geocoder.nominatim || {};

  /**
   * Attach geocoder input for Nominatim.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches views geocoder input for Nominatim to relevant elements.
   */
  Drupal.behaviors.geolocationGeocoderNominatim = {
    attach: function (context) {
      $.each(drupalSettings.geolocation.geocoder.nominatim.inputIds, function (index, inputId) {
        var geocoderInput = $('input.geolocation-geocoder-address[data-source-identifier="' + inputId + '"]', context);

        if (geocoderInput.length === 0) {
          return;
        }

        if (geocoderInput.hasClass('geocoder-attached')) {
          return;
        }
        else {
          geocoderInput.addClass('geocoder-attached');
        }

        geocoderInput.autocomplete({
          autoFocus: true,
          source: function (request, response) {
            var autocompleteResults = [];

            var options = {
              q: request.term,
              limit: 3,
              format: 'json'
            };

            var lang = $('html').attr('lang');
            if ($.inArray(lang, ['de', 'en', 'it', 'fr']) !== -1) {
              options.lang = lang;
            }

            if (typeof drupalSettings.geolocation.geocoder.nominatim.locationPriority !== 'undefined') {
              $.extend(options, drupalSettings.geolocation.geocoder.nominatim.locationPriority);
            }

            $.getJSON(
                'https://nominatim.openstreetmap.org',
                options,
                function (data) {

                  /**
                   * @param {int} index
                   * @param {PhotonResult} result
                   */
                  $.each(data, function (index, result) {
                    var formatted_address = [];
                    if (typeof result.display_name !== 'undefined') {
                      formatted_address.push(result.display_name);
                    }

                    autocompleteResults.push({
                      value: result.display_name, //+ ' - ' + formatted_address.join(', '),
                      result: result
                    });
                  });
                  response(autocompleteResults);
                }
            );
          },

          /**
           * Option form autocomplete selected.
           *
           * @param {Object} event - See jquery doc
           * @param {Object} ui - See jquery doc
           * @param {Object} ui.item - See jquery doc
           */
          select: function (event, ui) {
            Drupal.geolocation.geocoder.resultCallback({
                geometry: {
                  location: {
                    lat: function () {
                      return ui.item.result.lat;
                    },
                    lng: function () {
                      return ui.item.result.lon;
                    }
                  },
                  bounds: ui.item.result.properties
                }
            }, $(event.target).data('source-identifier').toString());
          }
        })
        .on('input', function () {
          Drupal.geolocation.geocoder.clearCallback($(this).data('source-identifier').toString());
        });

      });
    },
    detach: function () {}
  };

})(jQuery, Drupal);
